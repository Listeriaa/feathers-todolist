
# Installation

## Install backend
```
npm install (sur back et front)

npm i parcel -g (si pas installé)
```

## Create tables
```
cd backend
npm run create-tables
```

## Install frontend
```
npm run dev
```


# Execution

## Start server
```
cd backend
npm run dev
```

## Start client
```

parcel index.html

```

Open browser on localhost:8080
Open multiple tabs to see real-time effects

